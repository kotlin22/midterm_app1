package com.jirat.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock
import android.widget.TextView

class HelloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)
        val receiveMessage  = intent.getStringExtra(AlarmClock.EXTRA_MESSAGE)
        val textName = findViewById<TextView>(R.id.textViewShowName).apply {
            text = receiveMessage
        }
    }
}
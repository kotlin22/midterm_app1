package com.jirat.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.util.Log
import android.widget.TextView
import com.jirat.app1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val helloName: TextView = findViewById(R.id.textViewName)
        val helloCode: TextView = findViewById(R.id.textViewCode)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.buttonHello.setOnClickListener{
            Log.d(TAG, ""+helloName.text + " "+helloCode.text)
            val sendName = helloName.text
            val intent = Intent(this, HelloActivity::class.java).apply {
                putExtra(EXTRA_MESSAGE, sendName)
            }
            startActivity(intent)
        }
    }

    companion object {
        private const val TAG = "MainActivity"
    }
}